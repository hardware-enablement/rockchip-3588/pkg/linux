debian/gitignore.patch

# Disable features broken by exclusion of upstream files
debian/dfsg/arch-powerpc-platforms-8xx-ucode-disable.patch
debian/dfsg/drivers-media-dvb-dvb-usb-af9005-disable.patch
debian/dfsg/video-remove-nvidiafb-and-rivafb.patch
debian/dfsg/documentation-fix-broken-link-to-cipso-draft.patch

# Changes to support package build system
debian/version.patch
debian/uname-version-timestamp.patch
debian/kernelvariables.patch
debian/mips-boston-disable-its.patch
debian/mips-ieee754-relaxed.patch
debian/mips-enable-r2-to-r6-emu-by-default.patch
debian/arch-sh4-fix-uimage-build.patch
debian/tools-perf-perf-read-vdso-in-libexec.patch
debian/tools-perf-install-python-bindings.patch
debian/wireless-add-debian-wireless-regdb-certificates.patch
debian/export-symbols-needed-by-android-drivers.patch
debian/android-enable-building-ashmem-and-binder-as-modules.patch
debian/documentation-drop-sphinx-version-check.patch
debian/perf-traceevent-support-asciidoctor-for-documentatio.patch
debian/kbuild-look-for-module.lds-under-arch-directory-too.patch
debian/kbuild-abort-build-if-subdirs-used.patch
debian/makefile-make-compiler-version-comparison-optional.patch
bugfix/all/revert-tools-build-clean-cflags-and-ldflags-for-fixdep.patch
debian/fixdep-allow-overriding-hostcc-and-hostld.patch
debian/linux-perf-remove-remaining-source-filenames-from-executable.patch

# Fixes/improvements to firmware loading
features/all/drivers-media-dvb-usb-af9005-request_firmware.patch
debian/iwlwifi-do-not-request-unreleased-firmware.patch
debian/firmware_loader-log-direct-loading-failures-as-info-for-d-i.patch
bugfix/all/radeon-amdgpu-firmware-is-required-for-drm-and-kms-on-r600-onward.patch

# Change some defaults for security reasons
debian/af_802154-Disable-auto-loading-as-mitigation-against.patch
debian/rds-Disable-auto-loading-as-mitigation-against-local.patch
debian/dccp-disable-auto-loading-as-mitigation-against-local-exploits.patch
debian/hamradio-disable-auto-loading-as-mitigation-against-local-exploits.patch
debian/fs-enable-link-security-restrictions-by-default.patch

# Set various features runtime-disabled by default
debian/yama-disable-by-default.patch
debian/add-sysctl-to-disallow-unprivileged-CLONE_NEWUSER-by-default.patch
features/all/security-perf-allow-further-restriction-of-perf_event_open.patch
features/x86/intel-iommu-add-option-to-exclude-integrated-gpu-only.patch
features/x86/intel-iommu-add-kconfig-option-to-exclude-igpu-by-default.patch

# Disable autoloading/probing of various drivers by default
debian/cdc_ncm-cdc_mbim-use-ncm-by-default.patch
debian/snd-pcsp-disable-autoload.patch
bugfix/x86/viafb-autoload-on-olpc-xo1.5-only.patch
debian/fjes-disable-autoload.patch

# Taint if dangerous features are used
debian/fanotify-taint-on-use-of-fanotify_access_permissions.patch
debian/btrfs-warn-about-raid5-6-being-experimental-at-mount.patch

# Arch bug fixes
bugfix/arm/arm-dts-kirkwood-fix-sata-pinmux-ing-for-ts419.patch
bugfix/x86/perf-tools-fix-unwind-build-on-i386.patch
bugfix/sh/sh-boot-do-not-use-hyphen-in-exported-variable-name.patch
bugfix/arm/arm-mm-export-__sync_icache_dcache-for-xen-privcmd.patch
bugfix/powerpc/powerpc-boot-fix-missing-crc32poly.h-when-building-with-kernel_xz.patch
bugfix/arm64/arm64-acpi-Add-fixup-for-HPE-m400-quirks.patch

bugfix/x86/mfd-intel_soc_pmic_bxtwc-Use-IRQ-domain-for-USB-Type.patch
bugfix/x86/mfd-intel_soc_pmic_bxtwc-Use-IRQ-domain-for-TMU-devi.patch
bugfix/x86/mfd-intel_soc_pmic_bxtwc-Use-IRQ-domain-for-PMIC-dev.patch
bugfix/x86/mfd-intel_soc_pmic_bxtwc-Fix-IRQ-domain-names-duplic.patch

# Arch features
features/x86/x86-memtest-WARN-if-bad-RAM-found.patch
features/x86/x86-make-x32-syscall-support-conditional.patch

# Miscellaneous bug fixes
bugfix/all/disable-some-marvell-phys.patch
bugfix/all/fs-add-module_softdep-declarations-for-hard-coded-cr.patch
bugfix/all/documentation-use-relative-source-paths-in-abi-documentation.patch

# Miscellaneous features

# Lockdown missing pieces
features/all/lockdown/efi-add-an-efi_secure_boot-flag-to-indicate-secure-b.patch
features/all/lockdown/efi-lock-down-the-kernel-if-booted-in-secure-boot-mo.patch
features/all/lockdown/mtd-disable-slram-and-phram-when-locked-down.patch
features/all/lockdown/arm64-add-kernel-config-option-to-lock-down-when.patch

# Improve integrity platform keyring for kernel modules verification
features/all/db-mok-keyring/0003-MODSIGN-checking-the-blacklisted-hash-before-loading-a-kernel-module.patch
features/all/db-mok-keyring/KEYS-Make-use-of-platform-keyring-for-module-signature.patch
features/all/db-mok-keyring/trust-machine-keyring-by-default.patch

# Security fixes

# Fix exported symbol versions
bugfix/all/module-disable-matching-missing-version-crc.patch

# Tools bug fixes
bugfix/all/usbip-document-tcp-wrappers.patch
bugfix/all/kbuild-fix-recordmcount-dependency.patch
bugfix/all/tools-perf-remove-shebangs.patch
bugfix/x86/revert-perf-build-fix-libunwind-feature-detection-on.patch
bugfix/all/tools-build-remove-bpf-run-time-check-at-build-time.patch
bugfix/all/cpupower-fix-checks-for-cpu-existence.patch
bugfix/all/libapi-define-_fortify_source-as-2-not-empty.patch
bugfix/all/tools-perf-fix-missing-ldflags-for-some-programs.patch
bugfix/all/tools_lib_symbol_use_d_fortify_source_2_for_non_debug_builds.patch
bugfix/all/perf-tools-support-extra-cxxflags.patch
bugfix/all/perf-tools-pass-extra_cflags-through-to-libbpf-build-again.patch
bugfix/all/kbuild-bpf-fix-btf-reproducibility.patch
bugfix/all/libbpf-Add-missing-per-arch-include-path.patch

# debian-installer fixes
bugfix/powerpc/fbdev-offb-Update-expected-device-name.patch

# RK3588 patches
rk3588/0083-regulator-Add-of_regulator_get_optional-for-pure-DT-.patch
rk3588/0084-regulator-Add-devres-version-of-of_regulator_get_opt.patch
rk3588/0085-arm64-dts-rockchip-rk3588-rock5b-add-USB-C-support.patch
rk3588/0086-math.h-add-DIV_ROUND_UP_NO_OVERFLOW.patch
rk3588/0087-clk-divider-Fix-divisor-masking-on-64-bit-platforms.patch
rk3588/0088-clk-composite-replace-open-coded-abs_diff.patch
rk3588/0089-clk-rockchip-support-clocks-registered-late.patch
rk3588/0090-clk-rockchip-rk3588-register-GATE_LINK-later.patch
rk3588/0091-clk-rockchip-expose-rockchip_clk_set_lookup.patch
rk3588/0092-clk-rockchip-implement-linked-gate-clock-support.patch
rk3588/0093-clk-rockchip-rk3588-drop-RK3588_LINKED_CLK.patch
rk3588/0094-arm64-dts-rockchip-rk3588-evb1-add-bluetooth-rfkill.patch
rk3588/0095-arm64-dts-rockchip-rk3588-evb1-improve-PCIe-ethernet.patch
rk3588/0096-arm64-dts-rockchip-Slow-down-EMMC-a-bit-to-keep-IO-s.patch
rk3588/0097-vop2-Add-clock-resets-support.patch
rk3588/0098-arm64-dts-rockchip-Add-VOP-clock-resets-for-rk3588s.patch
rk3588/0099-dt-bindings-display-vop2-Add-VP-clock-resets.patch
rk3588/0100-media-v4l2-ctrls-core-Set-frame_mbs_only_flag-by-def.patch
rk3588/0101-media-rockchip-Move-H264-CABAC-table-to-header-file.patch
rk3588/0102-media-rockchip-Introduce-the-rkvdec2-driver.patch
rk3588/0103-media-dt-bindings-rockchip-Document-RK3588-Video-Dec.patch
rk3588/0104-arm64-dts-rockchip-Add-rkvdec2-Video-Decoder-on-rk35.patch
rk3588/0105-arm64-defconfig-enable-Rockchip-RK3588-video-decoder.patch
rk3588/0106-mfd-rk8xx-Fix-shutdown-handler.patch
rk3588/0107-WIP-phy-phy-rockchip-samsung-hdptx-Add-FRL-EARC-supp.patch
rk3588/0108-TESTING-phy-phy-rockchip-samsung-hdptx-Add-verbose-l.patch
rk3588/0109-WIP-dt-bindings-display-rockchip-drm-Add-optional-cl.patch
rk3588/0110-WIP-drm-rockchip-vop2-Improve-display-modes-handling.patch
rk3588/0111-arm64-dts-rockchip-Add-HDMI0-bridge-to-rk3588.patch
rk3588/0112-arm64-dts-rockchip-Enable-HDMI0-on-rock-5b.patch
rk3588/0113-arm64-dts-rockchip-Enable-HDMI0-on-rk3588-evb1.patch
rk3588/0114-WIP-arm64-dts-rockchip-Enable-HDMI0-PHY-clk-provider.patch
rk3588/0115-WIP-arm64-dts-rockchip-Make-use-of-HDMI0-PHY-PLL-on-.patch
rk3588/0116-WIP-arm64-dts-rockchip-Make-use-of-HDMI0-PHY-PLL-on-.patch
rk3588/0117-dt-bindings-display-bridge-Add-schema-for-Synopsys-D.patch
rk3588/0118-dt-bindings-display-rockchip-Add-schema-for-RK3588-H.patch
rk3588/0119-drm-bridge-synopsys-Add-DW-HDMI-QP-TX-controller-dri.patch
rk3588/0120-drm-rockchip-Add-basic-RK3588-HDMI-output-support.patch
rk3588/0121-arm64-defconfig-Enable-Rockchip-extensions-for-Synop.patch
rk3588/0122-MAINTAINERS-Add-entry-for-Synopsys-DesignWare-HDMI-R.patch
rk3588/0123-dt-bindings-media-Document-bindings-for-HDMI-RX-Cont.patch
rk3588/0124-arm64-dts-rockchip-Add-device-tree-support-for-HDMI-.patch
rk3588/0125-media-platform-synopsys-Add-support-for-HDMI-input-d.patch
rk3588/0126-arm64-defconfig-Enable-Synopsys-HDMI-receiver.patch
rk3588/0127-arm64-dts-rockchip-Enable-HDMI-receiver-on-rock-5b.patch
rk3588/0128-arm64-dts-rockchip-Enable-HDMI-receiver-on-RK3588-EV.patch
rk3588/0129-arm64-defconfig-Enable-default-EDID-for-Synopsys-HDM.patch
rk3588/0130-regulator-Add-devm_-of_regulator_get.patch
rk3588/0131-pmdomain-rockchip-cleanup-mutex-handling-in-rockchip.patch
rk3588/0132-pmdomain-rockchip-forward-rockchip_do_pmu_set_power_.patch
rk3588/0133-pmdomain-rockchip-reduce-indentation-in-rockchip_pd_.patch
rk3588/0134-dt-bindings-power-rockchip-add-regulator-support.patch
rk3588/0135-pmdomain-rockchip-add-regulator-support.patch
rk3588/0136-arm64-dts-rockchip-Add-GPU-power-domain-regulator-de.patch
rk3588/0137-dt-bindings-net-wireless-brcm4329-fmac-add-pci14e4-4.patch
rk3588/0138-dt-bindings-net-wireless-brcm4329-fmac-add-clock-des.patch
rk3588/0139-wifi-brcmfmac-Add-optional-lpo-clock-enable-support.patch
rk3588/0140-wifi-brcmfmac-add-flag-for-random-seed-during-firmwa.patch
rk3588/0141-arm64-dts-rockchip-rk3588-evb1-add-WLAN-controller.patch
rk3588/0142-arm64-dts-rockchip-add-and-enable-gpu-node-for-Radxa.patch
rk3588/0143-arm64-dts-rockchip-Enable-HDMI0-on-rock-5a.patch
rk3588/0144-phy-rockchip-samsung-hdptx-Set-drvdata-before-enabli.patch
